<?php

  class ModelPersonalPage extends Model
  {
    private $_db;

    public function __construct()
    {
      parent::__construct();
      $this->_db = $this->connect();
      $this->_db->exec("USE " . getenv("DB_NAME"));
    }

    public function addNewDatas($date, $exercice, $weightOrTime, $rep)
    {
      $null = 0;
      $stmt = $this->_db->prepare(
        "INSERT INTO exercices (userId, exName, exWeight, exTime, exRep, exDate) 
          VALUES (
            :userId,
            :exName,
            :exWeight,
            :exTime,
            :exRep,
            :exDate
        )");
      $stmt->bindParam(":userId", $_SESSION["userId"]);
      $stmt->bindParam(":exName", $exercice);
      if ($_SESSION['exWeight'] == 1) {
        $stmt->bindParam(":exWeight", $weightOrTime);
        $stmt->bindParam(":exTime", $null);
      } else {
        $stmt->bindParam(":exWeight", $null);
        $stmt->bindParam(":exTime", $weightOrTime);
      }
      $stmt->bindParam(":exRep", $rep);
      $stmt->bindParam(":exDate", substr($date, 0, 10));
      $stmt->execute();
    }

    public function fetchDatas()
    {
      $stmt = $this->_db->prepare("SELECT * FROM exercices WHERE userId = :id ORDER BY exDate DESC");
      $stmt->bindParam(":id", $_SESSION['userId']);
      $stmt->execute();
      $i = 0;
      while ($row = $stmt->fetch()) {
        if ($i++ == 0)
          $exActualDate = substr($row['exDate'], 4, 6);
        ?>
        </table>
        <?php if (substr($row['exDate'], 4, 6) == $exActualDate) :?>
          <table>
        <tr class="text-white">
          <td><?= $row['exName'] ?></td>
          <td><?= $row['exWeight'] . " kg" ?></td>
          <td><?= $row['exTime'] . " s" ?></td>
          <td><?= $row['exRep'] ?></td>
          <td><?= date("d-m-Y", strtotime($row['exDate'])) ?></td>
        </tr>
        <?php else :?>
          <div style="margin-top: 3rem;">
            <h3 class="text-info"><?= date("d-m-Y", strtotime($row['exDate']));?></h3> 
          </div>
        <?php $exActualDate = substr($row['exDate'], 4, 6); endif; ?>
        <?php
      }
    }
  }