  btnFr = document.getElementById('fr')
  btnEn = document.getElementById('en')

  btnFr.addEventListener('click', () => {
    document.getElementById('homeEn').style.display = 'none'
    document.getElementById('homeFr').style.display = 'block'
    btnEn.style.color = 'beige';
    btnFr.style.color = '#34B5E5';
  })
  btnEn.addEventListener('click', () => {
    document.getElementById('homeFr').style.display = 'none'
    document.getElementById('homeEn').style.display = 'block'
    btnFr.style.color = 'beige';
    btnEn.style.color = '#34B5E5';
  })