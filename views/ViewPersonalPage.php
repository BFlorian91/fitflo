<?php

  class ViewPersonalPage extends View
  {
    public function body()
    {
      ?>
        <div class="container fullHeight">
          <div class="row justify-content-center mb-4" id="btns">
            <button class="btn-design col-3 bg-danger" onclick="weight()">Weight</button>
            <button class="btn-design offset-1 col-3 bg-info" onclick="timer()">Time</button>
          </div>

          <!-- FORM 1 -->
          <div id="weight" style="display: none">
            <form class="text-center offset-md-3 col-md-6" method="post">
              <label for="exDate">Date</label>
              <input class="form-control mb-4" type="date" name="exDate">
              <label for="picker">Select Exercice</label>
              <select class="selectpicker form-control mb-4" name="picker">
                <option value="0">Bicepts curl</option>
                <option value="1">Chin-up (Traction supination)</option>
                <option value="2">Pull-up (Traction pronation)</option>
                <option value="3">Deeps</option>
                <option value="4">Triceps pushdown rope attachment (FRONT)</option>
                <option value="5">Triceps pushdown rope attachment (BACK)</option>
                <option value="6">Bench leg press</option>
              </select>
              <label for="exWeight">Weight</label>
              <input class="form-control mb-4" type="text" name="exWeight" placeholder="Poids en Kg">
              <lable for="exRep">Repeat</label>
              <input class="form-control mb-4" type="text" name="exRep" placeholder="Nombre de répétitions">
              <input class="form-control mb-4 offset-3 col-6" type="submit">
            </form>
          </div>

          <!-- FORM 2 -->
          <div id="timer" style="display:none">
            <form class="text-center offset-md-3 col-md-6" method="post">
              <label for="exDate">Date</label>
              <input class="form-control mb-4" type="date" name="exDate">
              <label for="picker">Select Exercice</label>
              <select class="selectpicker form-control mb-4" name="picker">
                <option value="0">Jumping jacks</option>
                <option value="1">High-stepping (Montée de genoux)</option>
                <option value="2">Gainage</option>
                <option value="3">Crunch (Sit up)</option>
                <option value="4">Leg lift (Relevé de jambe abdo)</option>
                <option value="5">Lunges (Fentes)</option>
              </select>
              <label for="exTime">Time</label>
              <input class="form-control mb-4" type="text" name="exTime" placeholder="Temps en secondes">
              <lable for="exRep">Repeat</label>
              <input class="form-control mb-4" type="text" name="exRep" placeholder="Nombre de répétitions">
              <input id="submit" class="form-control mb-4 offset-3 col-6" type="submit">
            </form>
          </div>
          <table class="table table-sm">
            <thead class="text-white">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Weight</th>
                <th scope="col">Time</th>
                <th scope="col">Repeat</th>
                <th scope="col">Date</th>
              </tr>
            </thead>
            <tbody id="datas">
            </tbody>
          </table>
          <script>
            // const btn = document.getElementById('submit')
            // btn.addEventListener('click', (e) => {
              // e.preventDefault();
              let ajax = new XMLHttpRequest();
              ajax.open("GET", "/getPersoDatas", true)
              //recieve response
              ajax.onreadystatechange = () => {
                if (ajax.readyState == 4 && ajax.status == 200) {
                  document.getElementById('datas').innerHTML = ajax.responseText
                }
              }
              //sending request
              ajax.send();

            // })
          </script>
          <script src="/lib/javascript/formStats.js"></script>
        </div>
      <?php
    }
  }