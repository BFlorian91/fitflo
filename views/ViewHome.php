<?php

  class ViewHome extends View
  {
    public function body()
    {
      ?>
        <div class="container fullHeight">
          <div class="d-flex flex-row-reverse reverse font-poppins">
            <p class="position-relative"><span id="fr" class="translater">FR</span><span class="translater mr-2 ml-2"> | </span><span id="en" class="translater" style="color: #34B5E5">EN</span></p>
          </div>
          <div id="homeEn" class="card mt-4">
          <div class="card-body text-white card-bg">
            <h1 class="card-title text-center font-poppins">What's <span class="text-info font-tw">FitFlo</span>?</h1>
            <p class="font-roboto" style="padding-bottom: 15px;"><span class="font-tw text-info" style="font-size: 1.2rem;">FitFlo</span> is a little web app for memorize your score at the gym and make some statistic.
               Some Feature will be add in the futur, like weight tracking,
               and maybe 'Challenge your friends' like a battle in the game Pokemon.
               A member can ask a fight with another member for to push back his limits, more information will be display here later...
            </p>
          </div>
          </div>
          <div id="homeFr" class="card textBox mt-4" style="display: none;">
            <div class="card-body text-white card-bg">
            <h1 class="card-title text-center font-poppins">Qu'est ce que <span class="text-info font-tw">FitFlo</span>?</h1>
            <p class="font-roboto" style="padding-bottom: 15px;"><span class="font-tw text-info" style="font-size: 1.2rem;">FitFlo</span> est une petite web application pour enregistrer vos score à la salle de sport et quelques autres statistiques.
              Plusieurs nouvelles options seront ajouté dans le futur, comme un traqueur de poids,
              et peut être 'Challenge tes amis' une sorte de bataille comme dans le jeu Pokemon.
              Un membre pourra demander un défi avec un autre membre du site pour qu'ils repoussent leurs limites, 
              plus d'information seront à venir...
            </p>
            </div>
          </div>
          <?php if (empty($_SESSION['token']) != '') :?>
            <a href="signin" class="btn btn-info btn-block my-4 col-6 offset-3">Sign in</a>
          <?php endif; ?>
        </div>
        <script src="/lib/javascript/home.js"></script>
      <?php
    }
  }