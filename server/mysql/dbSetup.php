<?php

  $dbName = getenv("DB_NAME");
  $port = getenv("DB_PORT");
  $user = getenv("DB_USER");
  $pass = getenv("DB_PASSWORD");


  // CREATE DATABASE
  try {
    $db = new PDO("mysql:host=fl_mysql;port" . $port, $user, $pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec("CREATE DATABASE IF NOT EXISTS " . $dbName);
    $db->exec("USE " . $dbName);
  } catch(PDOException $e) {
    echo $e->getmessage();
    die(-1);
  }


  //  CREATE USER TABLE
  try {
    $db->exec("CREATE TABLE IF NOT EXISTS users (
      id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
      username VARCHAR(50) NOT NULL,
      mail VARCHAR(100) NOT NULL,
      passwd VARCHAR(255) NOT NULL,
      token VARCHAR(255) NOT NULL
    )");
  } catch (PDOException $e) {
    die ($e->getMessage());
  }


  // CREATE EXERCICES TABLE
  try {
    $db->exec("CREATE TABLE IF NOT EXISTS exercices (
      id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
      userId INT NOT NULL,
      exName VARCHAR(100) NOT NULL,
      exWeight INT(4) NOT NULL DEFAULT 0,
      exTime INT(4) NOT NULL DEFAULT 0,
      exRep INT(3) NOT NULL,
      exDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
      FOREIGN KEY (userId) REFERENCES users(id)
    )");
  } catch (PDOException $e) {
    die($e->getMessage());
  }