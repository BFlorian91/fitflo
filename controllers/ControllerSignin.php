<?php

  class ControllerSignin extends Controller
  {
    public static function CreateView()
    {
      $view = new ViewSignin();
      $action = new ModelSignin();

      if (isset($_POST['username']) && isset($_POST['password'])) {
        $username = htmlspecialchars($_POST['username']);
        $password = htmlspecialchars($_POST['password']);
        if ($action->signin(ucFirst($username), $password)) {
          $view = new ViewPersonalPage();
        }
      }
      $view->build_page();
    }
  }