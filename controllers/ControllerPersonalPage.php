<?php

  class ControllerPersonalPage extends Controller
  {
    public function createView()
    {
      $view = new ViewPersonalPage();
      $action = new ModelPersonalPage();

      if (isset($_POST['exDate']) && isset($_POST['picker']) && (isset($_POST['exWeight']) || isset($_POST['exTime']))
       && isset($_POST['exRep']) ) {
      
        $_SESSION['exWeight'] = isset($_POST['exWeight']) ? "1" : "2";

        $exerciceW = ["Bicepts Curl", "Chin-up", "Pull-up", "Deeps", "Triceps Pushdown Rop Attachement (FRONT)", "Triceps Pushdown Rop Attachement (BACK)", "Bench leg press"];
        $exerciceT = ["Jumping Jacks", "High-stepping", "Gainage", "Crunch", "Leg lift", "Lunges"];
        $index = $_POST['picker'];

        $date = htmlspecialchars($_POST['exDate']);
        $exercice = htmlspecialchars(isset($_POST['exWeight']) ? $exerciceW[$index] : $exerciceT[$index]);
        $weightOrTime = htmlspecialchars(isset($_POST['exWeight']) ? intval(substr($_POST['exWeight'], 0, 4)) : intval(substr($_POST['exTime'], 0, 5)));
        $rep = htmlspecialchars(intval(substr($_POST['exRep'], 0, 3)));

        $action->addNewDatas($date, $exercice, $weightOrTime, $rep);
      }
      $view->build_page();
    }

    public function getPersonalDatas()
    {
      $action = new ModelPersonalPage();
      $action->fetchDatas();
    }
  }