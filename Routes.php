<?php

  Route::set('explore', function() {
    ControllerHome::createView();
  });

  Route::set('signin', function() {
    ControllerSignin::CreateView();
  });
  
  Route::set('signup', function() {
    ControllerSignup::CreateView();
  });

  Route::set('logout', function() {
    ControllerLogout::CreateView();
  });

  // Sort of API
  Route::set('getPersoDatas', function() {
    ControllerPersonalPage::getPersonalDatas();
  });
  //////////////
  
  Route::set($_SESSION['token'], function() {
    if (trim($_SESSION['token'])) {
      ControllerPersonalPage::CreateView();
    } else {
      ControllerSignin::CreateView();
    }
  });